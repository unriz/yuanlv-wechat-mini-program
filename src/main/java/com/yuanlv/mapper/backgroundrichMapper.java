package com.yuanlv.mapper;

import com.yuanlv.yuanlvDTO.Activity;
import com.yuanlv.yuanlvDTO.RichText;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface backgroundrichMapper {

    @Select("select rich_text from background  where background_id=#{backgroundId}")
    RichText get_rich(Integer backgroundId);
}
