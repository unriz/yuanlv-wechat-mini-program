package com.yuanlv.mapper;

import com.yuanlv.yuanlvDTO.Activity;
import com.yuanlv.yuanlvDTO.Recruit;
import com.yuanlv.yuanlvDTO.Recruit_T;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface Count_activity {
    @Insert("insert into count_activity(activity_id,person_id,max_sum)" +
            " VALUES" +
            " (#{activityId},#{personId},#{maxSum})")
    void recruit(Recruit_T recruitT);
}
