package com.yuanlv.mapper;

import com.yuanlv.yuanlvDTO.Activity;
import com.yuanlv.yuanlvDTO.Recruit_T;
import com.yuanlv.yuanlvDTO.RichText;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface richText {
    @Select("select rich_text from rich_text  where activity_id=#{activityId}")
    RichText get_rich(Integer activityId);
}