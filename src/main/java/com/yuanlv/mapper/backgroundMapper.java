package com.yuanlv.mapper;

import com.yuanlv.yuanlvDTO.Activity;
import com.yuanlv.yuanlvDTO.Background;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface backgroundMapper {
    @Select("select * from background_show")
    List<Background> search();

}
