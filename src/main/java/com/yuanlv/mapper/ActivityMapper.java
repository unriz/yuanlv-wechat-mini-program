package com.yuanlv.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import com.yuanlv.yuanlvDTO.Activity;

import java.util.List;

@Mapper
public interface ActivityMapper {
    @Select("select * from activity ")
    List<Activity> search();
}
