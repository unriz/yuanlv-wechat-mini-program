package com.yuanlv.mapper;

import com.yuanlv.yuanlvDTO.RichText;
import com.yuanlv.yuanlvDTO.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface userServiceMapper {
    @Select("select * from user  where open_id=#{openId}")
    User getByOpenId(String openId);

   // @Update("update user set   image=#{image},name=#{name}   where open_id = #{openId}")
    void updateById(User user);
//    ,name=#{name},image=#{image}

    @Insert("insert into user(open_id,name,image) values (#{openId},name=#{name},image=#{image})")
    void insert(User insertUser);
}
