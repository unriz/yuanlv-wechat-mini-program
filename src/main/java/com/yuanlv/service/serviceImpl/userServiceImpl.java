package com.yuanlv.service.serviceImpl;

import com.yuanlv.service.userService;
import com.yuanlv.yuanlvDTO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class userServiceImpl implements userService {
    @Autowired
    private com.yuanlv.mapper.userServiceMapper userServiceMapper;
    @Override
    public User selectByOpenId(String open_id) {
        return userServiceMapper.getByOpenId(open_id);
    }

    @Override
    public void updateById(User user) {
        userServiceMapper.updateById(user);
    }

    @Override
    public void insert(User insertUser) {
        userServiceMapper.insert(insertUser);
    }
}
