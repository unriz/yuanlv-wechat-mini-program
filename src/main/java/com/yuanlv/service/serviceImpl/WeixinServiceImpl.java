//package com.yuanlv.service.serviceImpl;
//
//import cn.hutool.core.lang.UUID;
//import cn.hutool.http.HttpUtil;
//import com.alibaba.fastjson.JSON;
//import com.crush.weixin.commons.RedisKey;
//import com.crush.weixin.commons.Result;
//import com.crush.weixin.entity.WXAuth;
//import com.crush.weixin.entity.Weixin;
//import com.crush.weixin.entity.WxUserInfo;
//import com.crush.weixin.mapper.WeixinMapper;
//import com.crush.weixin.service.IWeixinService;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.stereotype.Service;
//
//
///**
// * @author crush
// * @since 2021-09-14
// */
//@Slf4j
//@Service
//public class WeixinServiceImpl extends ServiceImpl<WeixinMapper, Weixin> implements IWeixinService {
//
//
//    @Value("${weixin.appid}")
//    private String appid;
//
//    @Value("${weixin.secret}")
//    private String secret;
//
//    @Autowired
//    StringRedisTemplate redisTemplate;
//
//    @Autowired
//    WxService wxService;
//
//
//    @Override
//    public String getSessionId(String code) {
//        String url = "https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}&js_code={2}&grant_type=authorization_code";
//        String replaceUrl = url.replace("{0}", appid).replace("{1}", secret).replace("{2}", code);
//        String res = HttpUtil.get(replaceUrl);
//        String s = UUID.randomUUID().toString();
//        redisTemplate.opsForValue().set(RedisKey.WX_SESSION_ID + s, res);
//        return s;
//    }
//
//    @Override
//    public Result authLogin(WXAuth wxAuth) {
//        try {
//            String wxRes = wxService.wxDecrypt(wxAuth.getEncryptedData(), wxAuth.getSessionId(), wxAuth.getIv());
//            log.info("用户信息："+wxRes);
//            //用户信息：{"openId":"o20","nickName":"juana","gender":2,"language":"zh_CN","city":"Changsha","province":"Hunan","country":"China","avatarUrl":"头像链接","watermark":{"timestamp":dsfs,"appid":"应用id"}}
//            WxUserInfo wxUserInfo = JSON.parseObject(wxRes,WxUserInfo.class);
//            // 业务操作：你可以在这里利用数据 对数据库进行查询， 如果数据库中没有这个数据，就添加进去，即实现微信账号注册
//            // 如果是已经注册过的，就利用数据，生成jwt 返回token，实现登录状态
//            return Result.SUCCESS(wxUserInfo);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return Result.FAIL();
//    }
//}
