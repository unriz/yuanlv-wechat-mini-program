package com.yuanlv.service.serviceImpl;

import com.yuanlv.service.activityService;
import com.yuanlv.yuanlvDTO.Recruit;
import com.yuanlv.yuanlvDTO.Recruit_T;
import com.yuanlv.yuanlvDTO.RichText;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuanlv.yuanlvDTO.Activity;

import java.util.List;

@Service
public class ActivityImpl implements activityService {
    @Autowired
    private com.yuanlv.mapper.ActivityMapper activityMapper;
    @Autowired
    private com.yuanlv.mapper.Count_activity countActivity;
    @Autowired
    private com.yuanlv.mapper.richText richText;
    @Override
    public List<Activity> search() {
        return activityMapper.search();
    }

    /**
     * 更新报名信息
     */
    @Override
    public void update(Recruit recruit) {
        Recruit_T recruitT = new Recruit_T();
        //属性拷贝
        BeanUtils.copyProperties(recruit, recruitT);
            countActivity.recruit(recruitT);
    }

    @Override
    public RichText get_rich(Integer activityId) {
        return richText.get_rich(activityId);
    }
}
