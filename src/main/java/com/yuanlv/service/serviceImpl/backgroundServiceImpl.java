package com.yuanlv.service.serviceImpl;

import com.yuanlv.mapper.backgroundMapper;
import com.yuanlv.mapper.backgroundrichMapper;
import com.yuanlv.service.backgroundService;
import com.yuanlv.yuanlvDTO.Background;
import com.yuanlv.yuanlvDTO.RichText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class backgroundServiceImpl implements backgroundService {
    @Autowired
    private backgroundMapper backgroundMapper;
    @Autowired
    private backgroundrichMapper backgroundrichmapper;
    @Override
    public List<Background> search() {
        return backgroundMapper.search();
    }

    @Override
    public RichText get_rich(Integer backgroundId) {
        return backgroundrichmapper.get_rich(backgroundId);
    }
}
