package com.yuanlv.service;

import com.yuanlv.yuanlvDTO.Activity;
import com.yuanlv.yuanlvDTO.Background;
import com.yuanlv.yuanlvDTO.Recruit;
import com.yuanlv.yuanlvDTO.RichText;

import java.util.List;

public interface backgroundService {

    public List<Background> search();


    //void update(Recruit recruit);

    RichText get_rich(Integer backgroundId);
}
