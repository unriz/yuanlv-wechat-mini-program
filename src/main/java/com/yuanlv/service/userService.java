package com.yuanlv.service;

import com.yuanlv.yuanlvDTO.User;
import org.springframework.stereotype.Service;

@Service
public interface userService {
    User selectByOpenId(String open_id);

    void updateById(User user);

    void  insert(User insertUser);
}
