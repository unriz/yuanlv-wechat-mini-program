package com.yuanlv.service;

import com.yuanlv.yuanlvDTO.Recruit;
import com.yuanlv.yuanlvDTO.RichText;
import org.springframework.stereotype.Service;
import com.yuanlv.yuanlvDTO.Activity;

import java.util.List;


@Service
public interface activityService {

    public List<Activity> search();


    void update(Recruit recruit);

    RichText get_rich(Integer activityId);
}
