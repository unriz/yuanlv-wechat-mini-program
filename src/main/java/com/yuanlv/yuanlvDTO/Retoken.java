package com.yuanlv.yuanlvDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Retoken implements Serializable {
    private User user;
    private Session session;
}
