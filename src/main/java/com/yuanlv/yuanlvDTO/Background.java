package com.yuanlv.yuanlvDTO;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Background implements Serializable {
    private Long id;

    private String text;
    private Long backgroundId;
}
