package com.yuanlv.yuanlvDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Recruit_T {
    private Integer id;

    private Integer activityId;

    private Integer personId;

    private Integer maxSum;

}
