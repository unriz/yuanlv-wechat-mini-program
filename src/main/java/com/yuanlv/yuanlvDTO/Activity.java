package com.yuanlv.yuanlvDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Activity  implements Serializable {
    private Long id;
    private String authorName;

    private String text;

    private LocalDateTime CreateTime;

    private LocalDateTime DeadTime;
    private Long authorId;
    private Long activityId;


}
