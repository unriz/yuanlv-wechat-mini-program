package com.yuanlv.controller;

import com.yuanlv.Result.Result;
import com.yuanlv.Util.AliOssUtil;
import com.yuanlv.Util.HttpClientUtil;
import com.yuanlv.Util.JwtUtil;
import com.yuanlv.VO.UserLoginVO;
import com.yuanlv.config.FileUtil;
import com.yuanlv.config.UserConstantInterface;
import com.yuanlv.constant.JwtClaimsConstant;
import com.yuanlv.properties.JwtProperties;
import com.yuanlv.service.backgroundService;
import com.yuanlv.service.userService;
import com.yuanlv.yuanlvDTO.*;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

import static com.yuanlv.Result.Result.success;

@Slf4j
@RestController
public class Calculator {
    @Autowired
    private AliOssUtil aliOssUtil;
    //    @Autowired
//    IWeixinService weixinService;
    @Autowired
    private com.yuanlv.service.activityService activityService;

    @Autowired
    private userService userService;
    @Autowired
    private backgroundService backgroundservice;
    @Autowired
    private JwtProperties jwtProperties;
    @GetMapping("getlist")
    Result<RichText> rich_get(Integer activityId)
    {
        //List<Activity> activityList= activityService.search();
        RichText richText= activityService.get_rich(activityId);
        return success(richText);
    }

    @GetMapping("test")
    Result<List<Activity>> test()
    {
        //List<Activity> activityList= activityService.search();
        List<Activity> activityList= activityService.search();
        return success(activityList);
    }

    @GetMapping("background")
    Result<List<Background>> getbackground()
    {
        //List<Activity> activityList= activityService.search();
        List<Background> backgroundList= backgroundservice.search();
        return success(backgroundList);
    }
    @GetMapping("getbackgroundlist")
    Result<RichText> backgroundrich_get(Integer backgroundId)
    {
        //List<Activity> activityList= activityService.search();
        RichText richText= backgroundservice.get_rich(backgroundId);
        return success(richText);
    }
    @PostMapping("weixin/baoming")
    public Result<String> recruit(@RequestBody Recruit recruit)
    {
        //List<Activity> activityList= activityService.search();
        activityService.update(recruit);
        return success();
    }
    @PostMapping("updateImage")
    public Result<String> recruit(@RequestBody User user)
    {
        //List<Activity> activityList= activityService.search();
        log.info("记录user"+user.toString());
        userService.updateById(user);
        return success();
    }
    @PostMapping("updatename")
    public Result<String> updatename(@RequestBody User user)
    {
        //List<Activity> activityList= activityService.search();
        log.info("记录user"+user.toString());
        userService.updateById(user);
        return success();
    }
    @GetMapping ("search")
    public Result<User> search(String openId)
    {
        User user=userService.selectByOpenId(openId);
        return success(user);
    }
    private static final Logger LOG = LoggerFactory
            .getLogger(Calculator.class);

    @PostMapping("/upload")
    public Result<String> upload(MultipartFile file) {
        try{
            log.info("文件上传：{}",file);
            //原始文件名
            String filename = file.getOriginalFilename();
            //截取原始文件的后缀png
            String extension=filename.substring(filename.lastIndexOf('.'));
            String uuid=UUID.randomUUID().toString()+extension;


            //文件的访问路径
            String path=aliOssUtil.upload(file.getBytes(), uuid);
            return Result.success(path);
        }catch(IOException e){
            log.error("文件上传失败,{}",e);

        }
        return Result.error("文件上传失败");

    }

    @PostMapping("/Upload")
    public Result<String> uploadMusicFile(HttpServletRequest request, @RequestParam("file")MultipartFile[] files){
        log.info("进入上传...");
        String uploadPath="C:/Users/asus/Desktop/quding/miniprogram/pages/user-center/";//存放到本地路径（示例）
        String ans=null;
        if(files!=null && files.length>=1) {
            BufferedOutputStream bw = null;
            try {
                String fileName = files[0].getOriginalFilename();
                String UU=UUID.randomUUID().toString();
                //判断是否有文件
                if(StringUtils.isNoneBlank(fileName)) {
                    //输出到本地路径
                    File outFile = new File(uploadPath + UU+ FileUtil.getFileType(fileName)+".jpg");
                    ans= UU+ FileUtil.getFileType(fileName)+".jpg";
                    log.info("path=="+uploadPath + UU+ FileUtil.getFileType(fileName));
                    FileUtils.copyInputStreamToFile(files[0].getInputStream(), outFile);
                }
            } catch (Exception e)
            {
                e.printStackTrace();
            }
            finally {
                try {
                    if(bw!=null) {bw.close();}
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return success(ans);
    }

    @GetMapping("checklogin")
    public boolean token_check(String token)
    {
        //List<Activity> activityList= activityService.search();
        try {
            log.info("jwt校验:{}", token);
            Claims claims = JwtUtil.parseJWT(jwtProperties.getUserSecretKey(), token);
            String openId = (claims.get(JwtClaimsConstant.open_id).toString());
            //String sessionKey = (claims.get(JwtClaimsConstant.session_key).toString());
            log.info("当前用户的open_id：{}", openId);
            User user=userService.selectByOpenId(openId);
            if(user==null){
                return false;
            }
            //BaseContext.setCurrentId(userId);
            //3、通过，放行
            return true;
        } catch (Exception ex) {
            //4、不通过，响应401状态码

            return false;
        }
    }

    @PostMapping("/login")
    public Result<UserLoginVO> login(@RequestBody CodeDTO code){
        Map<String, String> param = new HashMap<>();
        param.put("appid", UserConstantInterface.WX_LOGIN_APPID);
        param.put("secret", UserConstantInterface.WX_LOGIN_SECRET);
        param.put("js_code", code.getCode());
        param.put("grant_type", UserConstantInterface.WX_LOGIN_GRANT_TYPE);
        // 发送请求
        String wxResult = HttpClientUtil.doGet(UserConstantInterface.WX_LOGIN_URL, param);

        JSONObject jsonObject = JSONObject.parseObject(wxResult);
        // 获取参数返回的
        //String session_key = jsonObject.get("session_key").toString();
        String open_id = jsonObject.get("openid").toString();
        // 根据返回的user实体类，判断用户是否是新用户，不是的话，更新最新登录时间，是的话，将用户信息存到数据库
        User user = userService.selectByOpenId(open_id);
        //System.out.println("user:"+user.toString());
        if(user==null)
        {
            User insert_user = new User();

            insert_user.setOpenId(open_id);

            System.out.println("insert_user:"+insert_user.toString());
            userService.insert(insert_user);

        }
        //为微信用户生成jwt令牌
        Map<String, Object> claims=new HashMap<>();
        claims.put(JwtClaimsConstant.open_id,open_id);
        //claims.put(JwtClaimsConstant.session_key,session_key);
        String token= JwtUtil.createJWT(jwtProperties.getUserSecretKey(),jwtProperties.getUserTtl(),claims);

        UserLoginVO userLoginVO = UserLoginVO.builder()
                .openid(open_id)
                .token(token)
                .build();
        return Result.success(userLoginVO);
        // 封装返回小程序
    }

}