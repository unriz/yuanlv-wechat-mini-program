package com.yuanlv.constant;

public class JwtClaimsConstant {

    public static final String open_id = "openId";
    public static final String session_key = "sessionKey";
    public static final String name = "name";
    public static final String image = "image";

}
